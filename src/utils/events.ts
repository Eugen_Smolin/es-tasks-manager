import { MouseEventHandler } from 'react';

export const stopPropagation: MouseEventHandler<HTMLElement> = (e): void => {
  e.stopPropagation();
};
