import { toast } from 'react-toastify';

export const handleError = (error: any) => {
  console.log(error.request);
  console.log(error.response);
  toast.error('Something went wrong!');
};
