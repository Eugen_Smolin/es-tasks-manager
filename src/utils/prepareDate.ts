import { format } from 'date-fns';

export const getDateFormat = (
  timestamp: number,
  dateFormat = 'dd-MM-yyyy HH:mm:ss'
): string => {
  if (!timestamp) return 'Invalid date';
  return format(new Date(timestamp), dateFormat);
};

export const getDaysInMonth = (dateFormat = 'dd MMM'): string[] => {
  const currentDate = new Date();
  const year = currentDate.getFullYear();
  const month = currentDate.getMonth();
  const firstDay = new Date(year, month, 1);
  const lastDay = new Date(year, month + 1, 0);
  const days = [];

  while (firstDay <= lastDay) {
    const dayFormat = format(new Date(firstDay), dateFormat);
    days.push(dayFormat);
    firstDay.setDate(firstDay.getDate() + 1);
  }

  return days;
};
