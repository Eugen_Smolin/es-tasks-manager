import styled, { css } from 'styled-components';

import { scrollBar } from 'components/AppTheme/GlobalStyled';

export const FormGroup = styled.div`
  position: relative;
  width: 100%;
  max-width: 420px;
  margin-bottom: 12px;
`;

export const Label = styled.label`
  margin-bottom: 8px;
`;

export const Field = styled.div`
  position: relative;
  width: 100%;
  height: 34px;
  padding: 4px 12px;
  font-size: 0.95rem;
  font-weight: 400;
  line-height: 1.5;
  color: ${({ theme }) => theme.textField.color};
  background-color: ${({ theme }) => theme.textField.bg};
  border: 1px solid ${({ theme }) => theme.textField.border};
  border-radius: 4px;
  cursor: pointer;
`;

export const FieldIcon = styled.div<{ isActive: boolean }>`
  display: flex;
  position: absolute;
  top: 9px;
  right: 8px;

  ${({ isActive }) =>
    isActive &&
    css`
      transform: rotate(180deg);
    `};
`;

export const Dropdown = styled.div`
  position: absolute;
  top: calc(100% + 4px);
  width: 100%;
  max-height: 240px;
  padding: 8px 0;
  color: ${({ theme }) => theme.select.dropdown.color};
  background-color: ${({ theme }) => theme.select.dropdown.bg};
  border-radius: 4px;
  overflow-y: auto;
  z-index: 10;
  ${scrollBar};
`;

export const Item = styled.div<{ isActive: boolean }>`
  height: 30px;
  padding: 4px 12px;
  cursor: pointer;
  transition: background-color 0.15s ease;

  ${({ isActive }) =>
    isActive &&
    css`
      background-color: ${({ theme }) => theme.primary};
    `};

  &:hover {
    background-color: ${({ theme }) => theme.primary};
  }
`;
