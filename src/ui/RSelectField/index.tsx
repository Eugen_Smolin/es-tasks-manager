import { useMemo, useRef, useState } from 'react';

import { ReactComponent as IconArrowSvg } from 'assets/img/icon-arrow.svg';
import { useOutsideClick } from 'hooks/useOutsideClick';
import { FormGroup, Label, Field, FieldIcon, Dropdown, Item } from './styled';

interface IOptions {
  value: string;
  label: string;
}

interface RSelectFieldProps {
  label: string;
  value: string;
  onChange: (value: string) => void;
  options: IOptions[];
}

export const RSelectField = ({
  label,
  value,
  onChange = () => {},
  options = [],
}: RSelectFieldProps) => {
  const [isVisible, setVisible] = useState(false);
  const dropdownRef = useRef<HTMLDivElement>(null);

  const onSelect = (val: string) => {
    onChange(val);
    setVisible(false);
  };

  useOutsideClick(dropdownRef, () => {
    if (isVisible) setVisible(false);
  });

  const fieldValue = useMemo(() => {
    const current = options.find((option) => option.value === value);
    return current?.label;
  }, [value]);

  return (
    <FormGroup>
      <Label>{label}</Label>
      <Field onClick={() => setVisible(true)}>
        {fieldValue}
        <FieldIcon isActive={isVisible}>
          <IconArrowSvg />
        </FieldIcon>
      </Field>
      {isVisible && (
        <Dropdown ref={dropdownRef}>
          {options.map((option) => (
            <Item
              key={option.value}
              isActive={option.value === value}
              onClick={() => onSelect(option.value)}
            >
              {option.label}
            </Item>
          ))}
        </Dropdown>
      )}
    </FormGroup>
  );
};
