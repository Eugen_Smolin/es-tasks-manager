import styled from 'styled-components';

export const Dialog = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${({ theme }) => theme.dialog.bg};
  z-index: 20;
`;

export const Popup = styled.div`
  position: relative;
  width: 100%;
  max-width: 420px;
  min-height: 170px;
  background-color: ${({ theme }) => theme.box.bg};
  border: 1px solid ${({ theme }) => theme.box.border};
  border-radius: 4px;
`;

export const Title = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 8px 16px;
  font-size: 18px;
  font-weight: 500;
  text-transform: uppercase;
  color: ${({ theme }) => theme.primary};
  background-color: ${({ theme }) => theme.box.bg};
  border-bottom: 1px solid ${({ theme }) => theme.box.border};
  border-radius: 2px 2px 0 0;
`;

export const Body = styled.div`
  padding: 16px;
`;

export const Footer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  column-gap: 16px;
  padding: 16px;
`;
