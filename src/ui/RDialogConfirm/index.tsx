import { ReactNode } from 'react';
import { createPortal } from 'react-dom';

import { RBtn, RLoader, ROverlay } from 'ui';
import { stopPropagation } from 'utils/events';
import { Dialog, Popup, Title, Body, Footer } from './styled';

interface RDialogConfirmProps {
  title: string;
  onConfirm: () => void;
  onCancel: () => void;
  loading?: boolean;
  children: ReactNode;
}

export const RDialogConfirm = ({
  title,
  onConfirm,
  onCancel,
  loading,
  children,
}: RDialogConfirmProps) => {
  const render = (
    <Dialog onClick={onCancel}>
      <Popup onClick={stopPropagation}>
        {loading && (
          <ROverlay>
            <RLoader />
          </ROverlay>
        )}
        <Title>{title}</Title>
        <Body>{children}</Body>
        <Footer>
          <RBtn
            variant="success"
            onClick={onConfirm}
          >
            Confirm
          </RBtn>
          <RBtn
            variant="danger"
            onClick={onCancel}
          >
            Cancel
          </RBtn>
        </Footer>
      </Popup>
    </Dialog>
  );

  return createPortal(render, document.getElementById('dialog-portal')!);
};
