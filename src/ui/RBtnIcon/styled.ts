import styled from 'styled-components';

import { TAppColors } from 'types/enum';

export const Icon = styled.button<{ variant: TAppColors }>`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 34px;
  height: 34px;
  padding: 4px;
  color: ${({ theme }) => theme.light};
  background-color: ${({ theme, variant }) => theme[variant]};
  border: 1px solid ${({ theme, variant }) => theme[variant]};
  border-radius: 2px;
  user-select: none;
  cursor: pointer;
  transition: box-shadow 0.15s ease;

  &:hover {
    box-shadow: 0 0 4px 4px ${({ theme, variant }) => `${theme[variant]}80`};
  }
`;
