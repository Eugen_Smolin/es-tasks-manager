import { FC, ReactNode } from 'react';

import { TAppColors } from 'types/enum';
import { Icon } from './styled';

interface RBtnIconProps {
  variant: TAppColors;
  onClick: () => void;
  children: ReactNode;
}

export const RBtnIcon: FC<RBtnIconProps> = ({
  variant = 'primary',
  onClick = () => {},
  children,
}) => {
  return (
    <Icon
      type="button"
      variant={variant}
      onClick={onClick}
    >
      {children}
    </Icon>
  );
};
