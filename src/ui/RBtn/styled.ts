import styled from 'styled-components';

import { TAppColors } from 'types/enum';

export const Button = styled.button<{ variant: TAppColors }>`
  display: inline-block;
  user-select: none;
  color: ${({ theme }) => theme.btn.color};
  background-color: ${({ theme, variant }) => theme[variant]};
  border: 1px solid ${({ theme, variant }) => theme[variant]};
  padding: 4px 12px;
  border-radius: 2px;
  text-transform: uppercase;
  transition: box-shadow 0.15s ease;

  &:hover {
    box-shadow: 0 0 4px 4px ${({ theme, variant }) => `${theme[variant]}80`};
  }
`;
