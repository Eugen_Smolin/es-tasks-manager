import { ButtonHTMLAttributes, FC, ReactNode } from 'react';

import { TAppColors } from 'types/enum';
import { Button } from './styled';

interface RBtnProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  type?: 'button' | 'submit';
  variant?: TAppColors;
  onClick?: () => void;
  children: ReactNode;
}

export const RBtn: FC<RBtnProps> = ({
  type = 'button',
  variant = 'primary',
  onClick = () => {},
  children,
}) => {
  return (
    <Button
      type={type}
      variant={variant}
      onClick={onClick}
    >
      {children}
    </Button>
  );
};
