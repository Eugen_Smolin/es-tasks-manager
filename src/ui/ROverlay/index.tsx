import { Overlay } from './styled';
import { FC, ReactNode } from 'react';

interface ROverlayProps {
  children: ReactNode;
}

export const ROverlay: FC<ROverlayProps> = ({ children }) => {
  return <Overlay>{children}</Overlay>;
};
