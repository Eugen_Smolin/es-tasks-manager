import { ReactNode } from 'react';
import { createPortal } from 'react-dom';

import { RBtn, RLoader, ROverlay } from 'ui';
import { stopPropagation } from 'utils/events';
import { Dialog, Popup, Title, Body, Footer } from './styled';

interface RDialogProps {
  title: ReactNode;
  onSave: () => void;
  onClose: () => void;
  loading?: boolean;
  children: ReactNode;
}

export const RDialog = ({
  title = 'Create task',
  onSave,
  onClose,
  loading,
  children,
}: RDialogProps) => {
  const render = (
    <Dialog onClick={onClose}>
      <Popup onClick={stopPropagation}>
        {loading && (
          <ROverlay>
            <RLoader />
          </ROverlay>
        )}
        <Title>{title}</Title>
        <Body>{children}</Body>
        <Footer>
          <RBtn
            variant="success"
            onClick={onSave}
          >
            Save
          </RBtn>
          <RBtn
            variant="danger"
            onClick={onClose}
          >
            Cancel
          </RBtn>
        </Footer>
      </Popup>
    </Dialog>
  );

  return createPortal(render, document.getElementById('dialog-portal')!);
};
