import { ChangeEvent, FC } from 'react';

import { FormGroup, Label, Input } from './styled';

interface RTextFieldProps {
  label: string;
  value: string;
  onChange?: (value: string) => void;
  disabled?: boolean;
}

export const RTextField: FC<RTextFieldProps> = ({
  label,
  value,
  onChange,
  disabled,
}) => {
  const onChangeValue = (e: ChangeEvent<HTMLInputElement>) => {
    if (onChange) onChange(e.target.value);
  };
  return (
    <FormGroup>
      <Label>{label}</Label>
      <Input
        type="text"
        value={value}
        onChange={!disabled ? onChangeValue : undefined}
        disabled={disabled}
      />
    </FormGroup>
  );
};
