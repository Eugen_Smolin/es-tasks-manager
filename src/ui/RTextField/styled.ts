import styled from 'styled-components';

export const FormGroup = styled.div`
  width: 100%;
  max-width: 420px;
  margin-bottom: 12px;
`;

export const Label = styled.label`
  margin-bottom: 4px;
`;

export const Input = styled.input`
  display: block;
  width: 100%;
  height: 34px;
  padding: 4px 12px;
  font-size: 0.95rem;
  font-weight: 400;
  line-height: 1.5;
  color: ${({ theme }) => theme.textField.color};
  background-color: ${({ theme }) => theme.textField.bg};
  border: 1px solid ${({ theme }) => theme.textField.border};
  border-radius: 4px;
  transition: border-color 0.15s ease, background-color 0.15s ease;

  &:focus {
    background-color: ${({ theme }) => theme.textField.focus.bg};
    border: 1px solid ${({ theme }) => theme.textField.focus.border};
  }

  &[disabled] {
    color: ${({ theme }) => theme.textField.disabled.color};
  }
`;
