import styled, { keyframes } from 'styled-components';

const rotation = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

export const Loader = styled.div`
  display: inline-block;
  width: 40px;
  height: 40px;
  margin: 0 auto;
  border-radius: 50%;
  border-top: 2px solid ${({ theme }) => theme.light};
  border-right: 2px solid transparent;
  animation: ${rotation} 1s linear infinite;

  &::after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    width: 40px;
    height: 40px;
    border-radius: 50%;
    border-left: 2px solid ${({ theme }) => theme.primary};
    border-bottom: 2px solid transparent;
    animation: ${rotation} 0.5s linear infinite reverse;
  }
`;
