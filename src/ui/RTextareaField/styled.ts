import styled from 'styled-components';

export const FormGroup = styled.div`
  width: 100%;
  max-width: 420px;
  margin-bottom: 12px;
`;

export const Label = styled.label`
  margin-bottom: 8px;
`;

export const Textarea = styled.textarea`
  display: block;
  width: 100%;
  height: auto;
  padding: 4px 12px;
  font-size: 0.95rem;
  font-weight: 400;
  line-height: 1.5;
  color: ${({ theme }) => theme.textField.color};
  background-color: ${({ theme }) => theme.textField.bg};
  border: 1px solid ${({ theme }) => theme.textField.border};
  border-radius: 4px;
  resize: vertical;
  transition: border-color 0.15s ease, background-color 0.15s ease;

  &:focus {
    background-color: ${({ theme }) => theme.textField.focus.bg};
    border: 1px solid ${({ theme }) => theme.textField.focus.border};
  }
`;
