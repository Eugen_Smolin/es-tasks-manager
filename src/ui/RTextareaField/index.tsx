import { ChangeEvent, FC } from 'react';

import { FormGroup, Label, Textarea } from './styled';

interface RTextareaFieldProps {
  label: string;
  value: string;
  onChange: (value: string) => void;
}

export const RTextareaField: FC<RTextareaFieldProps> = ({
  label,
  value,
  onChange,
}) => {
  const onChangeField = (e: ChangeEvent<HTMLTextAreaElement>) => {
    onChange(e.target.value);
  };

  return (
    <FormGroup>
      <Label>{label}</Label>
      <Textarea
        rows={8}
        value={value}
        onChange={onChangeField}
      ></Textarea>
    </FormGroup>
  );
};
