import { Navigate, Route, Routes } from 'react-router-dom';
import { useAuthState } from 'react-firebase-hooks/auth';

import { LoginPage } from 'pages/LoginPage';
import { NotFoundPage } from 'pages/NotFoundPage';
import { AppFooter } from 'components/AppFooter';
import { RLoader, ROverlay } from 'ui';
import { auth } from 'firebase.config';
import { useAppSelector } from 'hooks/useReduxHooks';
import { authSlice } from 'store/auth';
import { StatusEnum } from 'types/enum';
import { Layout, Container } from './styled';

export const AuthLayout = () => {
  const [user] = useAuthState(auth);
  const status = useAppSelector(authSlice.selectors.signInWithGoogleStatus);

  if (user) {
    return (
      <Navigate
        replace
        to="/app/dashboard"
      />
    );
  }

  return (
    <Layout>
      <Container>
        <Routes>
          <Route
            path="/"
            element={<LoginPage />}
          />
          <Route
            path="*"
            element={<NotFoundPage />}
          />
        </Routes>
      </Container>
      <AppFooter />

      {status === StatusEnum.PENDING && (
        <ROverlay>
          <RLoader />
        </ROverlay>
      )}
    </Layout>
  );
};
