import styled from 'styled-components';

export const Layout = styled.div`
  display: grid;
  margin: 0 16px;
  grid-template-columns: 260px auto;
  grid-template-rows: 74px 1fr auto;
  grid-template-areas:
    'header header'
    'sidebar main'
    'footer footer';
  min-height: 100vh;
`;

export const Main = styled.main`
  grid-area: main;
  display: flex;
  flex-direction: column;
  background-color: ${({ theme }) => theme.main.bg};
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
  padding: 16px;
`;
