import { Navigate, Route, Routes } from 'react-router-dom';
import { useAuthState } from 'react-firebase-hooks/auth';

import { DashboardPage } from 'pages/DashboardPage';
import { TasksListPage } from 'pages/TasksListPage';
import { ProjectsPage } from 'pages/ProjectsPage';
import { NotFoundPage } from 'pages/NotFoundPage';
import { SettingsPage } from 'pages/SettingsPage';
import { AppHeader } from 'components/AppHeader';
import { AppFooter } from 'components/AppFooter';
import { AppSidebar } from 'components/AppSidebar';
import { RLoader, ROverlay } from 'ui';
import { auth } from 'firebase.config';
import { useAppSelector } from 'hooks/useReduxHooks';
import { authSlice } from 'store/auth';
import { StatusEnum } from 'types/enum';
import { Layout, Main } from './styled';

export const AppLayout = () => {
  const [user] = useAuthState(auth);
  const status = useAppSelector(authSlice.selectors.signOutStatus);

  if (!user) {
    return (
      <Navigate
        replace
        to="/"
      />
    );
  }

  return (
    <Layout>
      <AppHeader />
      <AppSidebar />
      <Main>
        <Routes>
          <Route
            path="/dashboard"
            element={<DashboardPage />}
          />
          <Route
            path="/tasks"
            element={<TasksListPage />}
          />
          <Route
            path="/projects"
            element={<ProjectsPage />}
          />
          <Route
            path="/settings"
            element={<SettingsPage />}
          />
          <Route
            path="*"
            element={<NotFoundPage />}
          />
        </Routes>
      </Main>
      <AppFooter />

      {status === StatusEnum.PENDING && (
        <ROverlay>
          <RLoader />
        </ROverlay>
      )}
    </Layout>
  );
};
