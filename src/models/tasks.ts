import { TLocalTask } from 'types/tasks';

export const TaskModel = (data: TLocalTask): TLocalTask => ({
  id: data.id,
  title: data.title,
  body: data.body,
  status: data.status,
  createdAt: data.createdAt,
});
