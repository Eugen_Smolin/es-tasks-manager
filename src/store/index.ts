import { configureStore, combineReducers } from '@reduxjs/toolkit';

import { authReducer } from 'store/auth';
import { dashboardReducer } from 'pages/DashboardPage/store';
import { tasksListReducer } from 'pages/TasksListPage/store';

const rootReducer = combineReducers({
  authReducer,
  dashboardReducer,
  tasksListReducer,
});

export const store = configureStore({
  reducer: rootReducer,
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
