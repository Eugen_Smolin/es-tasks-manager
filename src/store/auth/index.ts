import { createSlice } from '@reduxjs/toolkit';

import { StatusEnum } from 'types/enum';
import { selectors } from './selectors';
import { thunks } from './thunks';
import { TAuthSlice } from './types';

const initialState: TAuthSlice = {
  signInWithGoogleStatus: StatusEnum.IDLE,
  signOutStatus: StatusEnum.IDLE,
};

const slice = createSlice({
  name: 'auth',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(thunks.signInWithGoogleThunk.pending, (state) => {
        state.signInWithGoogleStatus = StatusEnum.PENDING;
      })
      .addCase(thunks.signInWithGoogleThunk.fulfilled, (state) => {
        state.signInWithGoogleStatus = StatusEnum.SUCCESS;
      })
      .addCase(thunks.signInWithGoogleThunk.rejected, (state) => {
        state.signInWithGoogleStatus = StatusEnum.FAIL;
      })

      .addCase(thunks.signOutThunk.pending, (state) => {
        state.signOutStatus = StatusEnum.PENDING;
      })
      .addCase(thunks.signOutThunk.fulfilled, (state) => {
        state.signOutStatus = StatusEnum.SUCCESS;
      })
      .addCase(thunks.signOutThunk.rejected, (state) => {
        state.signOutStatus = StatusEnum.FAIL;
      });
  },
});

export const authSlice = {
  actions: slice.actions,
  selectors,
  thunks,
};

export const authReducer = slice.reducer;
