import { RootState } from 'store';

export const selectors = {
  signInWithGoogleStatus: (state: RootState) =>
    state.authReducer.signInWithGoogleStatus,
  signOutStatus: (state: RootState) => state.authReducer.signOutStatus,
};
