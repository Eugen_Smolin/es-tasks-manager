import { createAsyncThunk } from '@reduxjs/toolkit';
import {
  getAuth,
  GoogleAuthProvider,
  signInWithPopup,
  signOut,
} from 'firebase/auth';

import { auth } from 'firebase.config';
import { handleError } from 'utils/handleError';
import { toast } from 'react-toastify';

const signInWithGoogleThunk = createAsyncThunk(
  'auth/signInWithGoogle',
  async () => {
    try {
      const provider = new GoogleAuthProvider();
      await signInWithPopup(auth, provider);
    } catch (err) {
      handleError(err);
      throw err;
    }
  }
);

const signOutThunk = createAsyncThunk('auth/signOut', async () => {
  try {
    const auth = getAuth();
    await signOut(auth);
    toast.success('Sign-out successful!');
  } catch (err) {
    handleError(err);
    throw err;
  }
});

export const thunks = {
  signInWithGoogleThunk,
  signOutThunk,
};
