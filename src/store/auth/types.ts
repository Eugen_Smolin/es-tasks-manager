import { TStatusEnum } from 'types/enum';

export type TAuthSlice = {
  signInWithGoogleStatus: TStatusEnum;
  signOutStatus: TStatusEnum;
};
