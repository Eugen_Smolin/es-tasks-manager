import styled from 'styled-components';

export const Button = styled.button`
  display: flex;
  align-items: center;
  max-width: 240px;
  height: 50px;
  background-color: #4285f4;
  border: 2px solid #4285f4;
`;

export const Icon = styled.div`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  width: 46px;
  height: 46px;
  font-size: 24px;
  background-color: ${({ theme }) => theme.light};
`;

export const Text = styled.div`
  font-size: 18px;
  font-weight: 500;
  color: #ffffff;
  padding: 4px 12px;
`;
