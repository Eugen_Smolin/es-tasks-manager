import { FcGoogle } from 'react-icons/fc';

import { useAppDispatch } from 'hooks/useReduxHooks';
import { authSlice } from 'store/auth';
import { Button, Icon, Text } from './styled';

export const SignInWithGoogle = () => {
  const dispatch = useAppDispatch();

  const onLogin = () => {
    dispatch(authSlice.thunks.signInWithGoogleThunk());
  };

  return (
    <Button onClick={onLogin}>
      <Icon>
        <FcGoogle />
      </Icon>
      <Text>Sign in with Google</Text>
    </Button>
  );
};
