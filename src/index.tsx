import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';

import { App } from 'App';
import { Router } from 'router';
import { store } from 'store';
import { AppTheme } from 'components/AppTheme';
import { AppToast } from 'components/AppToast';
import { GlobalStyles } from 'components/AppTheme/GlobalStyled';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
  // <React.StrictMode>
  <Provider store={store}>
    <AppTheme>
      <GlobalStyles />
      <Router>
        <App />
      </Router>
      <AppToast />
    </AppTheme>
  </Provider>
  // </React.StrictMode>
);
