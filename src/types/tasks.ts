import { TTaskStatus } from './enum';

export type TLocalTask = {
  id: number;
  title: string;
  body: string;
  status: string;
  createdAt: number; // timestamp
};

export type TLocalTaskCreated = {
  id: number;
  status: TTaskStatus;
  createdAt: number;
};
