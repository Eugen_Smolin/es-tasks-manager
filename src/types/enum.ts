export const StatusEnum = {
  IDLE: 'idle',
  PENDING: 'pending',
  SUCCESS: 'success',
  FAIL: 'fail',
} as const;
export type TStatusEnum = (typeof StatusEnum)[keyof typeof StatusEnum];

export const TaskStatusEnum = {
  todo: 'TODO',
  in_progress: 'IN_PROGRESS',
  done: 'DONE',
} as const;

export type TTaskStatus = (typeof TaskStatusEnum)[keyof typeof TaskStatusEnum];

export type TAppColors =
  | 'primary'
  | 'secondary'
  | 'success'
  | 'info'
  | 'warning'
  | 'danger';
