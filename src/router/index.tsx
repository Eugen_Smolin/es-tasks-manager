import { FC, ReactNode } from 'react';
import { BrowserRouter } from 'react-router-dom';

interface IRouter {
  children: ReactNode;
}

export const Router: FC<IRouter> = ({ children }) => {
  return <BrowserRouter>{children}</BrowserRouter>;
};
