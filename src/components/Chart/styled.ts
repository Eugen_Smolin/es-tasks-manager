import styled from 'styled-components';

export const Wrap = styled.div`
  position: relative;
  background: ${({ theme }) => theme.box.bg};
  border: 1px solid ${({ theme }) => theme.box.border};
  width: 100%;
  height: 400px;
`;

export const Data = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  font-size: 16px;
  font-style: italic;
  font-weight: 300;
  text-transform: uppercase;
`;
