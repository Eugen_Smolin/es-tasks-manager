import { FC } from 'react';
import Chart, { Props } from 'react-apexcharts';

import { RLoader, ROverlay } from 'ui';
import { Wrap, Data } from './styled';

interface ApexChartProps extends Props {
  loading: boolean;
}

export const ApexChart: FC<ApexChartProps> = ({
  type = 'area',
  options = {},
  series = [],
  loading,
}) => {
  return (
    <Wrap>
      {loading && (
        <ROverlay>
          <RLoader />
        </ROverlay>
      )}
      {!series.length ? (
        <Data>Data is empty</Data>
      ) : (
        <Chart
          type={type}
          options={options}
          series={series}
          height={380}
        />
      )}
    </Wrap>
  );
};
