import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export const AppToast = () => {
  return (
    <ToastContainer
      position="top-center"
      autoClose={3000}
      newestOnTop={false}
      closeButton={false}
      // limit={1}
      pauseOnHover
      theme="dark"
    />
  );
};
