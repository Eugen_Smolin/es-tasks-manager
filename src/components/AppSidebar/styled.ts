import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

export const Sidebar = styled.aside`
  grid-area: sidebar;
  display: flex;
  flex-direction: column;
  width: 100%;
  background-color: ${({ theme }) => theme.sidebar.bg};
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
`;

export const Title = styled.div`
  color: ${({ theme }) => theme.font.secondary};
  font-weight: 700;
  text-transform: uppercase;
  padding: 15px 20px 5px 20px;
`;

export const LinkEl = styled(NavLink)`
  display: flex;
  align-items: center;
  padding: 10px 30px 10px 20px;
  color: ${({ theme }) => theme.font.base};
  transition: color 0.25s ease;

  &:hover,
  &.active {
    color: ${({ theme }) => theme.sidebar.linkHover};
    text-decoration: none;
  }
`;

export const Icon = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 10px;
  font-size: 1.4em;
  line-height: 1.4em;
  text-align: center;
  flex-shrink: 0;
`;
