import {
  TfiDashboard,
  TfiCheckBox,
  TfiHarddrives,
  TfiSettings,
} from 'react-icons/tfi';

import { Sidebar, Title, LinkEl, Icon } from './styled';

export const links = [
  { link: '/app/dashboard', name: 'Dashboard', icon: <TfiDashboard /> },
  { link: '/app/tasks', name: 'Tasks', icon: <TfiCheckBox /> },
  { link: '/app/projects', name: 'Projects', icon: <TfiHarddrives /> },
  { link: '/app/settings', name: 'Settings', icon: <TfiSettings /> },
];

export const AppSidebar = () => {
  return (
    <Sidebar>
      <Title>CATEGORIES</Title>
      {links.map((item) => (
        <LinkEl
          key={item.link}
          to={item.link}
        >
          <Icon>{item.icon}</Icon>
          {item.name}
        </LinkEl>
      ))}
    </Sidebar>
  );
};
