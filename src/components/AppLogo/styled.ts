import styled from 'styled-components';

export const Logo = styled.div`
  display: flex;
  align-items: center;
  width: 100px;

  & > svg {
    width: 100%;
    height: auto;

    path {
      fill: #ffffff;
    }
  }
`;
