import { ReactComponent as LogoSvg } from 'assets/img/logo.svg';
import { Logo } from './styled';

export const AppLogo = () => {
  return (
    <Logo>
      <LogoSvg />
    </Logo>
  );
};
