import styled, { keyframes } from 'styled-components';

const eat = (color: string) => keyframes`
  0% , 49% { border-right-color: ${color} }
  50% , 100% { border-right-color: transparent }
`;

const move = keyframes`
  0% { left: 75px ; opacity: 1}
  50% { left: 0; opacity: 1 }
  52%, 100% { left: -5px; opacity: 0; }
`;

export const Wrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
`;

export const Loader = styled.div`
  position: relative;
  border: 24px solid ${({ theme }) => theme.primary};
  border-radius: 50%;
  box-sizing: border-box;
  animation: ${({ theme }) => eat(theme.primary)} 1s linear infinite;

  &::after,
  &::before {
    content: '';
    position: absolute;
    left: 50px;
    top: 50%;
    transform: translateY(-50%);
    background: ${({ theme }) => theme.primary};
    width: 15px;
    height: 15px;
    border-radius: 50%;
    box-sizing: border-box;
    opacity: 0;
    animation: ${move} 2s linear infinite;
  }

  &::before {
    animation-delay: 1s;
  }
`;
