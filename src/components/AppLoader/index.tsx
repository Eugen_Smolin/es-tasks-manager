import React from 'react';

import { Wrap, Loader } from './styled';

export const AppLoader = () => {
  return (
    <Wrap>
      <Loader />
    </Wrap>
  );
};
