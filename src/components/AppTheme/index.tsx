import { FC, ReactNode } from 'react';
import { ThemeProvider } from 'styled-components';

const theme = {
  primary: '#1791ba',
  secondary: '#6C757D',
  success: '#17BA91',
  info: '#4DC3FF',
  warning: '#E8BA30',
  danger: '#E84A67',
  light: '#F8F9FA',
  dark: '#343A40',
  font: {
    base: '#adb5bd',
    secondary: '#dee2e6',
  },
  app: {
    color: '#adb5bd',
    bg: '#181b1e',
    border: '#282d31',
  },
  sidebar: {
    bg: '#212529',
    linkHover: '#ffffff',
  },
  main: {
    bg: '#2a2f34',
  },
  header: {
    bg: '#181b1e',
  },
  footer: {
    bg: '#181b1e',
  },
  btn: {
    color: '#f8f9fa',
    bg: '#1791ba',
    hover: '#137698',
  },
  select: {
    color: '#ced4da',
    bg: '#212529',
    border: '#495057',
    dropdown: {
      color: '#e9ecef',
      bg: '#495057',
    },
  },
  textField: {
    color: '#ced4da',
    bg: '#212529',
    border: '#495057',
    focus: {
      bg: '#42484f',
      border: '#65caec',
    },
    disabled: {
      color: '#75808b',
    },
  },
  box: {
    bg: '#212529',
    border: '#495057',
  },
  taskCard: {
    color: '#f8f9fa',
    bg: '#343a40',
    border: '#495057',
  },
  dialog: {
    bg: '#21252970',
  },
};

interface ThemeProps {
  children: ReactNode;
}

export const AppTheme: FC<ThemeProps> = ({ children }) => {
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};

export type CustomTheme = typeof theme;
