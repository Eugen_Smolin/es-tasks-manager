import styled from 'styled-components';

type TTypography = {
  display?: string;
  fz?: string;
  fw?: number;
  align?: string;
  transform?: string;
};

export const Typography = styled.div<TTypography>`
  display: ${({ display = 'block' }) => display};
  font-size: ${({ fz = '14px' }) => fz};
  font-style: normal;
  font-weight: ${({ fw = 400 }) => fw};
  text-align: ${({ align = 'start' }) => align};
  text-transform: ${({ transform = 'none' }) => transform};
  line-height: 142%;
`;
