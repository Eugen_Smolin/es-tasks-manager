import { css } from 'styled-components';

export const Base = css`
  body {
    font-family: 'Mukta', sans-serif;
    font-style: normal;
    font-weight: 400;
    font-size: 0.95rem;
    line-height: 1.5;
    letter-spacing: 0.2px;
    color: ${({ theme }) => theme.app.color};
    background-color: ${({ theme }) => theme.app.bg};
  }
`;
