import styled from 'styled-components';

export const Wrap = styled.footer`
  grid-area: footer;
  background: ${({ theme }) => theme.footer.bg};
  padding: 20px 0 10px;
  //margin-top: auto;
  text-align: center;
`;

export const Links = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  column-gap: 40px;
  margin-bottom: 16px;
`;

export const Link = styled.a`
  color: ${({ theme }) => theme.primary};
`;
