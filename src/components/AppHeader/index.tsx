import { Link } from 'react-router-dom';
import { TfiUser } from 'react-icons/tfi';
import { FiLogOut } from 'react-icons/fi';

import { AppLogo } from 'components/AppLogo';
import { useAppDispatch } from 'hooks/useReduxHooks';
import { authSlice } from 'store/auth';
import { Header, LinkToHome, Icons, Block } from './styled';

export const AppHeader = () => {
  const dispatch = useAppDispatch();

  const logout = () => {
    dispatch(authSlice.thunks.signOutThunk());
  };

  return (
    <Header>
      <LinkToHome to="/">
        <AppLogo />
      </LinkToHome>
      <Icons>
        <Link to="/app/settings">
          <Block>
            <TfiUser />
          </Block>
        </Link>
        <Block
          title="Logout"
          onClick={logout}
        >
          <FiLogOut />
        </Block>
      </Icons>
    </Header>
  );
};
