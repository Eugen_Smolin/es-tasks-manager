import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Header = styled.header`
  grid-area: header;
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: calc(100% - 16px);
  height: 74px;
  background-color: ${({ theme }) => theme.header.bg};
  z-index: 10;
`;

export const LinkToHome = styled(Link)`
  display: flex;
  align-items: center;

  &:hover {
    text-decoration: none;
  }
`;

export const Icons = styled.div`
  display: flex;
  align-items: center;
`;

export const Block = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 74px;
  height: 74px;
  font-size: 20px;
  color: ${({ theme }) => theme.font.base};
  border-left: 1px solid ${({ theme }) => theme.app.border};
  cursor: pointer;
  transition: color 0.15s ease;

  &:hover {
    color: ${({ theme }) => theme.light};
  }
`;
