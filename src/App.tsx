import { Route, Routes } from 'react-router-dom';
import { useAuthState } from 'react-firebase-hooks/auth';

import { AppLoader } from 'components/AppLoader';
import { AppLayout } from 'layouts/AppLayout';
import { AuthLayout } from 'layouts/AuthLayout';
import { auth } from 'firebase.config';

export const App = () => {
  const [, loading] = useAuthState(auth);

  if (loading) return <AppLoader />;

  return (
    <Routes>
      <Route
        path="/app/*"
        element={<AppLayout />}
      />
      <Route
        path="/*"
        element={<AuthLayout />}
      />
    </Routes>
  );
};
