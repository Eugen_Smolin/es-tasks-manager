import { SignInWithGoogle } from 'modules/SignInWithGoogle';

export const LoginPage = () => {
  return (
    <>
      <SignInWithGoogle />
    </>
  );
};
