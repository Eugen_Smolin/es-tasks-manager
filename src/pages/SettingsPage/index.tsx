import { useAuthState } from 'react-firebase-hooks/auth';

import { RTextField } from 'ui';
import { auth } from 'firebase.config';
import { Head, Title, Wrap, Img, Data } from './styled';

export const SettingsPage = () => {
  const [user] = useAuthState(auth);

  return (
    <>
      <Head>
        <Title>Settings</Title>
      </Head>
      <Wrap>
        <Img
          src={user?.photoURL || ''}
          alt="Photo"
        />
        <Data>
          <RTextField
            label="Name"
            value={user?.displayName || ''}
            disabled
          />
          <RTextField
            label="Email"
            value={user?.email || ''}
            disabled
          />
          <RTextField
            label="Phone number"
            value={user?.phoneNumber || '-'}
            disabled
          />
        </Data>
      </Wrap>
    </>
  );
};
