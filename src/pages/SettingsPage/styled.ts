import styled from 'styled-components';

export const Head = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-bottom: 8px;
  margin-bottom: 16px;
  border-bottom: 1px solid ${({ theme }) => theme.box.border};
`;

export const Title = styled.div`
  color: ${({ theme }) => theme.primary};
  font-size: 16px;
  font-weight: 600;
  text-transform: uppercase;
`;

export const Wrap = styled.div`
  display: grid;
  grid-template-columns: 96px 1fr;
  grid-column-gap: 24px;
`;

export const Img = styled.img`
  width: 96px;
  height: 96px;
  border-radius: 50%;
`;

export const Data = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 12px;
`;
