import { ChartTasksCreated } from 'pages/DashboardPage/components/ChartTasksCreated';
import { ChartTasksAmount } from 'pages/DashboardPage/components/ChartTasksAmount';
import { Head, Title, Charts } from './styled';

export const DashboardPage = () => {
  return (
    <>
      <Head>
        <Title>Dashboard</Title>
      </Head>
      <Charts>
        <ChartTasksCreated />
        <ChartTasksAmount />
      </Charts>
    </>
  );
};
