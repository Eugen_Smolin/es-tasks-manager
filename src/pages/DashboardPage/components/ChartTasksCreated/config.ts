import { getDaysInMonth } from 'utils/prepareDate';

export const options = {
  chart: {
    id: 'basic-area',
    foreColor: '#ccc',
  },
  title: {
    text: 'Number of task created per month',
    margin: 10,
  },
  xaxis: {
    categories: getDaysInMonth(),
  },
  toolbar: {
    autoSelected: 'pan',
    show: false,
  },
  colors: ['#008ffb'], // '#008ffb', '#feb019', '#00e396'
  stroke: {
    width: 2,
  },
  tooltip: {
    theme: 'dark',
    x: {
      show: true,
      format: 'dd MMM YYYY',
      formatter: undefined,
    },
    z: {
      formatter: undefined,
      title: 'Size: ',
    },
  },
  markers: {
    size: 4,
    colors: '#212529',
    strokeColor: ['#008ffb'],
    strokeWidth: 2,
    hover: {
      size: 8,
    },
  },
  dataLabels: {
    enabled: false,
  },
  grid: {
    borderColor: '#999999',
    clipMarkers: false,
  },
  fill: {
    gradient: {
      enabled: true,
      opacityFrom: 0.7,
      opacityTo: 0,
    },
  },
};
