import { useEffect } from 'react';

import { ApexChart } from 'components/Chart';
import { useAppDispatch, useAppSelector } from 'hooks/useReduxHooks';
import { dashboardSlice } from 'pages/DashboardPage/store';
import { StatusEnum } from 'types/enum';
import { options } from './config';
import { Wrap } from './styled';

export const ChartTasksCreated = () => {
  const dispatch = useAppDispatch();
  const stats = useAppSelector(dashboardSlice.selectors.statsTasksCreated);
  const status = useAppSelector(dashboardSlice.selectors.fetchCreatedStatus);
  const series = [{ name: 'Amount', data: stats }];

  useEffect(() => {
    dispatch(dashboardSlice.thunks.getTasksCreatedThunk());
  }, []);

  return (
    <Wrap>
      <ApexChart
        type="area"
        options={options}
        series={series}
        loading={status === StatusEnum.PENDING}
      />
    </Wrap>
  );
};
