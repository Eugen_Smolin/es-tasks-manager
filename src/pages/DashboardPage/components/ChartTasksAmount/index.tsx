import { useEffect } from 'react';

import { ApexChart } from 'components/Chart';
import { useAppDispatch, useAppSelector } from 'hooks/useReduxHooks';
import { dashboardSlice } from 'pages/DashboardPage/store';
import { StatusEnum } from 'types/enum';
import { options } from './config';
import { Wrap } from './styled';

export const ChartTasksAmount = () => {
  const dispatch = useAppDispatch();
  const stats = useAppSelector(dashboardSlice.selectors.statsTasksAmount);
  const status = useAppSelector(dashboardSlice.selectors.fetchingStatus);
  const series = stats;

  useEffect(() => {
    dispatch(dashboardSlice.thunks.getStatsTasksThunk());
  }, []);

  return (
    <Wrap>
      <ApexChart
        type="pie"
        options={options}
        series={series}
        loading={status === StatusEnum.PENDING}
      />
    </Wrap>
  );
};
