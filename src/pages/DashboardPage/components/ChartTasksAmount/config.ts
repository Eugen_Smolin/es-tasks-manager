export const options = {
  chart: {
    id: 'basic-pie',
    foreColor: '#ccc',
  },
  labels: ['To do', 'In progress', 'Done'],
  title: {
    text: 'Current number of tasks',
    margin: 10,
  },
  colors: ['#008ffb', '#feb019', '#00e396'],
  stroke: {
    width: 2,
    colors: ['#ccc'],
  },
};
