import { RootState } from 'store';
import { createSelector } from '@reduxjs/toolkit';

import { getDaysInMonth, getDateFormat } from 'utils/prepareDate';
import { TaskStatusEnum } from 'types/enum';
import { TSortedStats } from './types';

const listCreated = (state: RootState) => state.dashboardReducer.listCreated;
const list = (state: RootState) => state.dashboardReducer.list;

export const selectors = {
  statsTasksCreated: createSelector([listCreated], (items) => {
    if (!Array.isArray(items) || items.length === 0) {
      return [];
    }

    const days = getDaysInMonth();
    const sortedStats: TSortedStats = Object.fromEntries(
      days.map((day) => [day, []])
    );

    items.forEach((task) => {
      const day = getDateFormat(task.createdAt, 'dd MMM');
      sortedStats[day].push(task.id);
    });

    return Object.values(sortedStats).map((value) => value.length);
  }),
  statsTasksAmount: createSelector([list], (items) => {
    if (!Array.isArray(items) || items.length === 0) {
      return [];
    }

    const sortedList = items.reduce(
      (acc, task) => {
        acc[task.status].push(task.id);
        return acc;
      },
      {
        [TaskStatusEnum.todo]: [],
        [TaskStatusEnum.in_progress]: [],
        [TaskStatusEnum.done]: [],
      } as TSortedStats
    );

    return Object.values(sortedList).map((value) => value.length);
  }),
  fetchingStatus: (state: RootState) => state.dashboardReducer.fetchingStatus,
  fetchCreatedStatus: (state: RootState) =>
    state.dashboardReducer.fetchCreatedStatus,
};
