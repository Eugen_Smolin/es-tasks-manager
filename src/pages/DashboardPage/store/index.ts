import { createSlice } from '@reduxjs/toolkit';

import { selectors } from './selectors';
import { thunks } from './thunks';
import { StatusEnum } from 'types/enum';
import { TTasksListSlice } from './types';

const initialState: TTasksListSlice = {
  list: [],
  listCreated: [],
  fetchingStatus: StatusEnum.PENDING,
  fetchCreatedStatus: StatusEnum.PENDING,
};

const slice = createSlice({
  name: 'dashboard',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(thunks.getStatsTasksThunk.pending, (state) => {
        state.fetchingStatus = StatusEnum.PENDING;
      })
      .addCase(thunks.getStatsTasksThunk.fulfilled, (state, { payload }) => {
        if (payload) state.list = payload;
        state.fetchingStatus = StatusEnum.SUCCESS;
      })
      .addCase(thunks.getStatsTasksThunk.rejected, (state) => {
        state.fetchingStatus = StatusEnum.FAIL;
      })
      .addCase(thunks.getTasksCreatedThunk.pending, (state) => {
        state.fetchCreatedStatus = StatusEnum.PENDING;
      })
      .addCase(thunks.getTasksCreatedThunk.fulfilled, (state, { payload }) => {
        if (payload) state.listCreated = payload;
        state.fetchCreatedStatus = StatusEnum.SUCCESS;
      })
      .addCase(thunks.getTasksCreatedThunk.rejected, (state) => {
        state.fetchCreatedStatus = StatusEnum.FAIL;
      });
  },
});

export const dashboardSlice = {
  actions: slice.actions,
  selectors,
  thunks,
};

export const dashboardReducer = slice.reducer;
