import { createAsyncThunk } from '@reduxjs/toolkit';
import { collection, getDocs } from 'firebase/firestore/lite';

import { TLocalTask, TLocalTaskCreated } from 'types/tasks';
import { db } from 'firebase.config';
import { handleError } from 'utils/handleError';

const getStatsTasksThunk = createAsyncThunk(
  'dashboardReducer/getStatsTasks',
  async () => {
    try {
      const tasksCol = collection(db, 'tasks');
      const taskSnapshot = await getDocs(tasksCol);
      const list = taskSnapshot.docs.map((doc) => doc.data());
      return list as TLocalTask[];
    } catch (err) {
      handleError(err);
      throw err;
    }
  }
);

const getTasksCreatedThunk = createAsyncThunk(
  'dashboardReducer/getStatsTasksCreated',
  async () => {
    try {
      const tasksCol = collection(db, 'tasks-created');
      const taskSnapshot = await getDocs(tasksCol);
      const list = taskSnapshot.docs.map((doc) => doc.data());
      return list as TLocalTaskCreated[];
    } catch (err) {
      handleError(err);
      throw err;
    }
  }
);

export const thunks = {
  getStatsTasksThunk,
  getTasksCreatedThunk,
};
