import { TStatusEnum } from 'types/enum';
import { TLocalTask, TLocalTaskCreated } from 'types/tasks';

export type TTasksListSlice = {
  list: TLocalTask[];
  listCreated: TLocalTaskCreated[];
  fetchingStatus: TStatusEnum;
  fetchCreatedStatus: TStatusEnum;
};

export type TSortedStats = Record<string, number[]>;
