import { useState } from 'react';

import { RBtn } from 'ui';
import { TasksList } from 'pages/TasksListPage/components/TasksList';
import { TaskHandle } from 'pages/TasksListPage/components/TaskHandle';
import { Head, Title } from './styled';

export const TasksListPage = () => {
  const [isTaskHandle, setTaskHandle] = useState(false);

  const onTaskHandle = () => {
    setTaskHandle((prev) => !prev);
  };

  return (
    <>
      <Head>
        <Title>Tasks board</Title>
        <RBtn onClick={onTaskHandle}>Add task</RBtn>
      </Head>
      <TasksList handleEditTask={onTaskHandle} />
      {isTaskHandle && <TaskHandle onCloseDialog={onTaskHandle} />}
    </>
  );
};
