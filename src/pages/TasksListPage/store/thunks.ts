import { createAsyncThunk } from '@reduxjs/toolkit';
import {
  collection,
  doc,
  getDocs,
  setDoc,
  deleteDoc,
} from 'firebase/firestore/lite';
import { toast } from 'react-toastify';

import { TLocalTask } from 'types/tasks';
import { db } from 'firebase.config';
import { handleError } from 'utils/handleError';

const getTasksListThunk = createAsyncThunk(
  'tasksListReducer/getTasksList',
  async () => {
    try {
      const tasksCol = collection(db, 'tasks');
      const taskSnapshot = await getDocs(tasksCol);
      const list = taskSnapshot.docs.map((doc) => doc.data());
      return list as TLocalTask[];
    } catch (err) {
      handleError(err);
      throw err;
    }
  }
);

const createTaskThunk = createAsyncThunk(
  'tasksListReducer/createTask',
  async (task: TLocalTask) => {
    const id = task.createdAt.toString();
    const taskCreated = {
      id: task.id,
      status: task.status,
      createdAt: task.createdAt,
    };

    try {
      await setDoc(doc(db, 'tasks', id), task);
      await setDoc(doc(db, 'tasks-created', id), taskCreated); // for statistic
      toast.success('Task was created successfully!');
      return task as TLocalTask;
    } catch (err) {
      handleError(err);
      throw err;
    }
  }
);

const updateTaskThunk = createAsyncThunk(
  'tasksListReducer/updateTask',
  async (task: TLocalTask) => {
    const id = task.createdAt.toString();

    try {
      await setDoc(doc(db, 'tasks', id), task);
      toast.success('Task was updated successfully!');
      return task as TLocalTask;
    } catch (err) {
      handleError(err);
      throw err;
    }
  }
);

const deleteTaskThunk = createAsyncThunk(
  'tasksListReducer/deleteTask',
  async (id: number) => {
    try {
      await deleteDoc(doc(db, 'tasks', id.toString()));
      toast.success('Task was deleted successfully!');
      return id;
    } catch (err) {
      handleError(err);
      throw err;
    }
  }
);

export const thunks = {
  getTasksListThunk,
  createTaskThunk,
  updateTaskThunk,
  deleteTaskThunk,
};
