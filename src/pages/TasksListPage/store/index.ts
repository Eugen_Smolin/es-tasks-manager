import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { selectors } from './selectors';
import { thunks } from './thunks';
import { StatusEnum } from 'types/enum';
import { TLocalTask } from 'types/tasks';
import { TTasksListSlice } from './types';

const initialState: TTasksListSlice = {
  list: [],
  editedTask: null,
  fetchingStatus: StatusEnum.PENDING,
  updatingStatus: StatusEnum.IDLE,
  deleteStatus: StatusEnum.IDLE,
};

const slice = createSlice({
  name: 'tasks',
  initialState,
  reducers: {
    SET_EDITED_TASK: (state, { payload }: PayloadAction<TLocalTask>) => {
      state.editedTask = payload;
    },
    RESET_EDITED_TASK: (state) => {
      state.editedTask = null;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(thunks.getTasksListThunk.pending, (state) => {
        state.fetchingStatus = StatusEnum.PENDING;
      })
      .addCase(thunks.getTasksListThunk.fulfilled, (state, { payload }) => {
        if (payload) state.list = payload;
        state.fetchingStatus = StatusEnum.SUCCESS;
      })
      .addCase(thunks.getTasksListThunk.rejected, (state) => {
        state.fetchingStatus = StatusEnum.FAIL;
      })

      .addCase(thunks.createTaskThunk.pending, (state) => {
        state.updatingStatus = StatusEnum.PENDING;
      })
      .addCase(thunks.createTaskThunk.fulfilled, (state, { payload }) => {
        if (payload) state.list.push(payload);
        state.updatingStatus = StatusEnum.SUCCESS;
      })
      .addCase(thunks.createTaskThunk.rejected, (state) => {
        state.updatingStatus = StatusEnum.FAIL;
      })

      .addCase(thunks.updateTaskThunk.pending, (state) => {
        state.updatingStatus = StatusEnum.PENDING;
      })
      .addCase(thunks.updateTaskThunk.fulfilled, (state, { payload }) => {
        if (payload) {
          const taskIdx = state.list.findIndex(({ id }) => id === payload.id);
          if (taskIdx !== -1) {
            state.list[taskIdx] = payload;
            state.editedTask = null;
          }
        }
        state.updatingStatus = StatusEnum.SUCCESS;
      })
      .addCase(thunks.updateTaskThunk.rejected, (state) => {
        state.updatingStatus = StatusEnum.FAIL;
      })

      .addCase(thunks.deleteTaskThunk.pending, (state) => {
        state.deleteStatus = StatusEnum.PENDING;
      })
      .addCase(thunks.deleteTaskThunk.fulfilled, (state, { payload }) => {
        if (payload) {
          state.list = state.list.filter(({ id }) => id !== payload);
        }
        state.deleteStatus = StatusEnum.SUCCESS;
      })
      .addCase(thunks.deleteTaskThunk.rejected, (state) => {
        state.deleteStatus = StatusEnum.FAIL;
      });
  },
});

export const tasksListSlice = {
  actions: slice.actions,
  selectors,
  thunks,
};

export const tasksListReducer = slice.reducer;
