import { createSelector } from '@reduxjs/toolkit';

import { TaskStatusEnum } from 'types/enum';
import { RootState } from 'store';
import { TSortedList } from './types';

const list = (state: RootState) => state.tasksListReducer.list;

export const selectors = {
  list,
  sortedList: createSelector([list], (items) => {
    const sortedList: TSortedList = {
      [TaskStatusEnum.todo]: [],
      [TaskStatusEnum.in_progress]: [],
      [TaskStatusEnum.done]: [],
    };
    items.forEach((task) => {
      sortedList[task.status].unshift(task);
    });
    return sortedList;
  }),
  editedTask: (state: RootState) => state.tasksListReducer.editedTask,
  fetchingStatus: (state: RootState) => state.tasksListReducer.fetchingStatus,
  updatingStatus: (state: RootState) => state.tasksListReducer.updatingStatus,
  deleteStatus: (state: RootState) => state.tasksListReducer.deleteStatus,
};
