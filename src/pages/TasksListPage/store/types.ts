import { TStatusEnum } from 'types/enum';
import { TLocalTask } from 'types/tasks';

export type TSortedList = {
  [key: string]: TLocalTask[];
};

export type TTasksListSlice = {
  list: TLocalTask[];
  editedTask: TLocalTask | null;
  fetchingStatus: TStatusEnum;
  updatingStatus: TStatusEnum;
  deleteStatus: TStatusEnum;
};
