import styled from 'styled-components';

import { TAppColors } from 'types/enum';

export const Card = styled.div<{ color: TAppColors }>`
  width: 100%;
  background-color: ${({ theme }) => theme.taskCard.bg};
  border: 1px solid ${({ theme }) => theme.taskCard.border};
  border-radius: 4px;
  cursor: pointer;
  transition: box-shadow 0.15s ease;

  &:hover {
    box-shadow: 0 0 4px 4px ${({ theme, color }) => `${theme[color]}50`};
  }
`;

export const Title = styled.div<{ color: TAppColors }>`
  text-transform: uppercase;
  padding: 8px 16px;
  color: ${({ theme, color }) => theme[color]};
  background-color: ${({ theme }) => theme.taskCard.bg};
  border-bottom: 1px solid ${({ theme }) => theme.taskCard.border};
  word-break: break-word;
`;

export const Body = styled.div`
  font-size: 14px;
  padding: 0 16px;
  margin: 16px 0;
  color: ${({ theme }) => theme.taskCard.color};
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
  overflow: hidden;
`;

export const Time = styled.div`
  padding: 0 16px 8px;
`;
