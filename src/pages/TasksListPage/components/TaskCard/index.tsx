import { TLocalTask } from 'types/tasks';
import { TAppColors } from 'types/enum';
import { getDateFormat } from 'utils/prepareDate';
import { Card, Title, Body, Time } from './styled';

interface TaskCardProps {
  task: TLocalTask;
  color: TAppColors;
  onClick: () => void;
}

export const TaskCard = ({ task, color, onClick }: TaskCardProps) => {
  return (
    <Card
      color={color}
      onClick={onClick}
    >
      <Title color={color}>{task.title}</Title>
      <Body>{task.body}</Body>
      <Time>{getDateFormat(task.createdAt)}</Time>
    </Card>
  );
};
