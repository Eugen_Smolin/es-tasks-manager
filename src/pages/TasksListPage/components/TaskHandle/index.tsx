import { FC, useEffect, useState } from 'react';
import { RiDeleteBin6Line } from 'react-icons/ri';

import {
  RBtnIcon,
  RDialog,
  RDialogConfirm,
  RSelectField,
  RTextareaField,
  RTextField,
} from 'ui';
import { useAppDispatch, useAppSelector } from 'hooks/useReduxHooks';
import { StatusEnum, TaskStatusEnum } from 'types/enum';
import { TLocalTask } from 'types/tasks';
import { tasksListSlice } from 'pages/TasksListPage/store';
import { Typography } from 'components/AppTheme/GlobalStyled/components';
import { statusOptions } from './config';

interface TaskHandleProps {
  onCloseDialog: () => void;
}

export const TaskHandle: FC<TaskHandleProps> = ({ onCloseDialog }) => {
  const dispatch = useAppDispatch();
  const editedTask = useAppSelector(tasksListSlice.selectors.editedTask);
  const updStatus = useAppSelector(tasksListSlice.selectors.updatingStatus);
  const delStatus = useAppSelector(tasksListSlice.selectors.deleteStatus);
  const [formData, setFormData] = useState<TLocalTask>({
    id: Date.now(),
    title: '',
    body: '',
    status: TaskStatusEnum.todo,
    createdAt: Date.now(),
  });
  const [isConfirmDialog, setConfirmDialog] = useState(false);

  useEffect(() => {
    if (editedTask) setFormData(editedTask);

    return () => {
      if (editedTask) dispatch(tasksListSlice.actions.RESET_EDITED_TASK());
    };
  }, []);

  const onChange = (name: string) => (val: string) => {
    setFormData((prev) => ({ ...prev, [name]: val }));
  };

  const onSave = async () => {
    if (editedTask) {
      await dispatch(tasksListSlice.thunks.updateTaskThunk(formData));
    } else {
      await dispatch(tasksListSlice.thunks.createTaskThunk(formData));
    }
    onCloseDialog();
  };

  const onDelete = async () => {
    await dispatch(tasksListSlice.thunks.deleteTaskThunk(formData.id));
    onCloseDialog();
  };

  const title = editedTask ? (
    <>
      Edit task
      <RBtnIcon
        variant="danger"
        onClick={() => setConfirmDialog(true)}
      >
        <RiDeleteBin6Line />
      </RBtnIcon>
    </>
  ) : (
    <>Create task</>
  );

  return (
    <>
      {!isConfirmDialog ? (
        <RDialog
          title={title}
          onSave={onSave}
          onClose={onCloseDialog}
          loading={updStatus === StatusEnum.PENDING}
        >
          <RTextField
            label="Title"
            value={formData.title}
            onChange={onChange('title')}
          />
          <RTextareaField
            label="Description"
            value={formData.body}
            onChange={onChange('body')}
          />
          <RSelectField
            label="Status"
            value={formData.status}
            onChange={onChange('status')}
            options={statusOptions}
          />
        </RDialog>
      ) : (
        <RDialogConfirm
          title="Delete Task"
          onConfirm={onDelete}
          onCancel={() => setConfirmDialog(false)}
          loading={delStatus === StatusEnum.PENDING}
        >
          Are you sure you want to delete this task&nbsp;
          <Typography
            display="inline"
            transform="uppercase"
          >
            &quot;{formData.title}&quot;
          </Typography>
          ?
        </RDialogConfirm>
      )}
    </>
  );
};
