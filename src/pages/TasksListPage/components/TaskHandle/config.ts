import { TaskStatusEnum } from 'types/enum';

export const statusOptions = [
  { value: TaskStatusEnum.todo, label: 'To do' },
  { value: TaskStatusEnum.in_progress, label: 'In progress' },
  { value: TaskStatusEnum.done, label: 'Done' },
];
