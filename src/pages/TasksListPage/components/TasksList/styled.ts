import styled from 'styled-components';

import { TAppColors } from 'types/enum';
import { scrollBar } from 'components/AppTheme/GlobalStyled';

export const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-column-gap: 24px;
  height: 100%;
`;

export const Column = styled.div`
  height: 100%;
  background-color: ${({ theme }) => theme.box.bg};
  border: 1px solid ${({ theme }) => theme.box.border};
  border-radius: 4px;
`;

export const ColumnHead = styled.div<{ color: TAppColors }>`
  font-weight: 700;
  padding: 8px 0;
  text-align: center;
  text-transform: uppercase;
  color: ${({ theme }) => theme.light};
  background-color: ${({ theme, color }) => theme[color]};
  border-bottom: 1px solid ${({ theme }) => theme.box.border};
  border-radius: 2px 2px 0 0;
`;

export const ColumnContent = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 16px;
  padding: 16px;
  height: 68vh; // TODO: fix
  overflow-y: auto;
  ${scrollBar};
`;
