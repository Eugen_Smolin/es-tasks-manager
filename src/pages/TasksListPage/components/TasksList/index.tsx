import { FC, useEffect } from 'react';

import { RLoader } from 'ui';
import { TaskCard } from 'pages/TasksListPage/components/TaskCard';
import { useAppDispatch, useAppSelector } from 'hooks/useReduxHooks';
import {
  StatusEnum,
  TaskStatusEnum,
  TTaskStatus,
  TAppColors,
} from 'types/enum';
import { TLocalTask } from 'types/tasks';
import { tasksListSlice } from 'pages/TasksListPage/store';
import { Column, ColumnContent, ColumnHead, Grid } from './styled';

interface TasksListProps {
  handleEditTask: () => void;
}

export const TasksList: FC<TasksListProps> = ({ handleEditTask }) => {
  const dispatch = useAppDispatch();
  const list = useAppSelector(tasksListSlice.selectors.list);
  const sortedList = useAppSelector(tasksListSlice.selectors.sortedList);
  const status = useAppSelector(tasksListSlice.selectors.fetchingStatus);

  useEffect(() => {
    if (!list.length) {
      dispatch(tasksListSlice.thunks.getTasksListThunk());
    }
  }, []);

  const handleTaskCard = (task: TLocalTask) => () => {
    dispatch(tasksListSlice.actions.SET_EDITED_TASK(task));
    handleEditTask();
  };

  const renderColumn = (
    colName: string,
    colKey: TTaskStatus,
    colColor: TAppColors
  ) => (
    <Column>
      <ColumnHead color={colColor}>
        {colName} ({sortedList[colKey]?.length || 0})
      </ColumnHead>
      <ColumnContent>
        {status === StatusEnum.PENDING ? (
          <RLoader />
        ) : (
          sortedList[colKey].map((task) => (
            <TaskCard
              key={task.id}
              task={task}
              color={colColor}
              onClick={handleTaskCard(task)}
            />
          ))
        )}
      </ColumnContent>
    </Column>
  );

  return (
    <Grid>
      {renderColumn('To do', TaskStatusEnum.todo, 'primary')}
      {renderColumn('In progress', TaskStatusEnum.in_progress, 'warning')}
      {renderColumn('Done', TaskStatusEnum.done, 'success')}
    </Grid>
  );
};
