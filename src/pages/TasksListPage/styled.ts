import styled from 'styled-components';

export const Head = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-bottom: 8px;
  margin-bottom: 16px;
  border-bottom: 1px solid ${({ theme }) => theme.box.border};
`;

export const Title = styled.div`
  color: ${({ theme }) => theme.primary};
  font-size: 16px;
  font-weight: 600;
  text-transform: uppercase;
`;
